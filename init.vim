call plug#begin('~/.local/share/nvim/plugged')

Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'                                               " bottom status bar
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install -all' }
Plug 'junegunn/fzf.vim'
Plug 'derekwyatt/vim-scala'                                                  " scala plugin
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-commentary'
Plug 'kien/rainbow_parentheses.vim'
Plug 'vim-scripts/grep.vim'
Plug 'roxma/vim-paste-easy'
Plug 'scrooloose/nerdtree'
Plug 'morhetz/gruvbox'
Plug 'rust-lang/rust.vim'
Plug 'elmcast/elm-vim'
Plug 'airblade/vim-rooter'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'easymotion/vim-easymotion'

call plug#end()

" easy motion prefix
map <Space> <Plug>(easymotion-prefix)

" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#ale#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='powerlineish'

" startify
let g:startify_bookmarks = [ {'c': '~/.config/nvim/init.vim'}, {'d': '~/Dev'}, {'f': '~/.config/fish/config.fish'} ]

set t_Co=256
set number " line numbers

let g:deoplete#enable_at_startup = 1
let g:NERDTreeWinSize = 50
let g:NERDTreeShowHidden=1
let g:rg_command = 'rg --vimgrep --hidden'
let g:ale_completion_enabled = 1

let g:scratch_autohide = 1
let g:scratch_insert_autohide = 0

colorscheme gruvbox

tnoremap <Esc> <C-\><C-n> " make esc work in terminal mode

" Easier mappings for movement between windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" fzf/ripgrep mappings
nnoremap <C-p>p :Files<CR>
nnoremap <C-p>h :Files ~<CR>
nnoremap <C-p>l :BLines<CR>
nnoremap <C-p>g :Rg<CR>

nnoremap <esc> :noh<CR><esc> " remove search highlight when pressing esc

nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>
nnoremap <C-f>c :bnext<CR>:bd#<CR> 

" rainbow parentheses always on
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Configuration for vim-scala
au BufRead,BufNewFile *.sbt set filetype=scala

autocmd FileType json syntax match Comment +\/\/.\+$+<Paste>

" Open new buffer with Startify
nnoremap <C-f>v :vnew<CR>:Startify<CR>
nnoremap <C-f>h :new<CR>:Startify<CR>
nnoremap <C-f>n :enew<CR>:Startify<CR>

map <C-t> :NERDTreeToggle<CR>
map <F5> :e! %<CR>

set shiftwidth=2
set autoindent
set smartindent

set autowriteall
